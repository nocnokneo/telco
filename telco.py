#!/usr/bin/env python3
import argparse
import os
import re
import sys

import pyperclip
from desktop_notifier import DesktopNotifier


def install_desktop_file():
    is_running_as_root = os.geteuid() == 0
    if is_running_as_root:
        data_dir = "/usr/share"
    else:
        data_dir = os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.local/share"))

    # make applications directory if it doesn't exist
    applications_dir = os.path.join(data_dir, "applications")
    if not os.path.exists(applications_dir):
        os.makedirs(os.path.join(data_dir, "applications"))

    desktop_path = os.path.join(applications_dir, "telco.desktop")
    program_path = os.path.abspath(sys.argv[0])
    desktop = f"""
[Desktop Entry]
Name=telco
Comment=Copy the phone number from a tel:XXX link to the system clipboard
Exec={program_path} %U
Terminal=false
Type=Application
Encoding=UTF-8
MimeType=x-scheme-handler/tel
X-KDE-Protocols=tel
Name[en_US]=Telco
Hidden=true
"""
    with open(desktop_path, "w") as f:
        f.write(desktop)


def main():
    parser = argparse.ArgumentParser(
        prog="telco",
        description="Takes a tel:XXX url and copies the XXX phone number to the clipboard",
    )
    parser.add_argument(
        "url",
        help="tel:XXX url",
        nargs="?",
    )
    parser.add_argument("--install", help="register as a handler for tel: links", action="store_true")

    args = parser.parse_args()

    if args.install:
        install_desktop_file()
        sys.exit(0)

    if not args.url:
        parser.print_help()
        sys.exit(1)

    TEL_RE = re.compile(r"tel:(.*)")
    match = TEL_RE.search(args.url)

    if not match:
        print("Invalid tel: link", file=sys.stderr)
        sys.exit(1)

    phone_number = match.group(1)
    pyperclip.copy(phone_number)

    # show a desktop notification
    notifier = DesktopNotifier(app_name=parser.prog, app_icon="call-start")
    notifier.send_sync(title="Phone number copied to clipboard", message=phone_number)


if __name__ == "__main__":
    main()
