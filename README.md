# Telco(py)

[![Latest Release](https://gitlab.com/nocnokneo/telco/-/badges/release.svg)](https://gitlab.com/nocnokneo/telco/-/releases)

Copy the phone number of a `tel:XXX` link to the clipboard. Intended to be
installed as a `x-scheme-handler/tel` MIME handler.

## Installation and Usage

1. [Download the binary](https://gitlab.com/nocnokneo/telco/-/releases)
2. Make it executable and install it:

   ```sh
   chmod +x ~/Downloads/telco
   ~/Downloads/telco --install
   ```

3. Open a webpage with a `tel:XXX` phone number link and click it. You now have
   the phone number on your clipboard ready to paste somewhere.
